import numpy as np
from numpy import random
import matplotlib.pyplot as plt

P = 0.1
current_x = 0
current_y = 0

MAP_LEN = 15
NUMACTION = 4

B = 1
alpha = 0.2
gamma = 0.9

c = -2
l = -20
r = 100

DIRS  = {'n':0, 'w':1, 's':2, 'e':3}
REWS = {0:c, 1:c ,2:l ,3:r, 4:-20}

env = np.genfromtxt("ENV1.map", delimiter=',')
Q = np.zeros((MAP_LEN, MAP_LEN, 4))

def reward(env_temp):
	global current_y
	global current_x
	return REWS[env_temp[current_x][current_y]]

def soft_max(a):
	global current_y
	global current_x
	global Q
	num = np.exp(B * Q[current_x][current_y][a])
	den = 0
	for i in range(NUMACTION):
		den += np.exp(B * Q[current_x][current_y][i])
	return num/den
		
def choose_dir(dir):
	if(random.random() > P):
		return dir
	return random.choice(['e', 'w', 's', 'n'])

def move(dir, cert):
	global current_x
	global current_y
	if(cert == 0):
		f_dir = choose_dir(dir)
	else:
		f_dir = dir

	if(f_dir == 'n'):
		current_y -= 1	
	if(f_dir == 's'):
		current_y += 1
	if(f_dir == 'e'):
		current_x += 1
	if(f_dir == 'w'):
		current_x -= 1

	#check the agent position	
	if(current_x > 14):
		current_x = 14
	if(current_x < 0):
		current_x = 0

	if(current_y > 14):
		current_y = 14
	if(current_y < 0):
		current_y = 0

	return (f_dir, current_x, current_y)

def test(init_x, init_y, Q):
	global current_x
	global current_y
	current_x = init_x
	current_y = init_y
	val = 0
	while(True):
		if(current_x == 14 and current_y == 7):
			print("well done")
			break
		else:
			direction = random.choice(a=['n', 'w', 's', 'e'], p=[soft_max(0), soft_max(1), soft_max(2), soft_max(3)])
			real_dir, current_x, current_y = move(direction, 1)
			val += reward(env)
			print(current_x, current_y, real_dir)

	return val

result = []
def train():
	# random init
	global current_x
	global current_y
	global Q
	direction = ''
	for i in range(3000):
		current_x = random.randint(0, 14)
		current_y = random.randint(0, 14)
		# current_x = 0
		# current_y = 0
		env1 = env.copy()

		# rewards = 0
		# flag = 0
		# if(current_y == 0 and current_x == 0):
		# 	flag = 1
		while(True):
			# print(current_x ,current_y ,direction)
			if(current_x == 14 and current_y == 7):
				break
			else:
				direction = random.choice(a=['n', 'w', 's', 'e'], p=[soft_max(0), soft_max(1), soft_max(2), soft_max(3)])
				old_x = current_x
				old_y = current_y
				real_dir, current_x, current_y = move(direction, 0)
				# print(current_x, current_y)
				# penalizing the old places
				env1[old_x][old_y] = 4
				# rewards += reward(env)
				# updating Q
				Q[old_x][old_y][DIRS[real_dir]] = Q[old_x][old_y][DIRS[real_dir]] + alpha * (reward(env1) + gamma * max(Q[current_x][current_y][0], 
										Q[current_x][current_y][1], Q[current_x][current_y][2] ,
											Q[current_x][current_y][3]) - Q[old_x][old_y][DIRS[real_dir]])
		# if(flag):
		# 	result.append(-60 + rewards)
				


train()
print("train is over")
# print(result)
# plt.plot(result)
# plt.show()
print("P = ", P, " the best val is: ",test(0, 0, Q))